# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [0.0.1] - 2020-02-11

### Added

- Set up a complete pipeline with best pratices regarding:
  - Documentation
  - Contributions files
  - CI/CD
  - Unit testing
  - Building packages
  - Auditing
  - Linting
  - Scripts helpers

[0.0.1]: https://gitlab.com/beescreens/beescreens-display-web-interface/-/tags/0.0.1
