# BeeScreens Display Web Interface
[![Pipeline Status][pipeline-status-badge]][pipeline-status-url]
[![Coverage Report][coverage-report-badge]][coverage-report-url]
[![License][license-badge]][license-url]
[![Code of Conduct][code-of-conduct-badge]][code-of-conduct-url]
[![Keep a Changelog v1.1.0 badge][changelog-badge]][changelog-url]
[![BeeScreens' Telegram Channel][telegram-channel-badge]][telegram-channel-url]
[![BeeScreens' Telegram Community][telegram-community-badge]][telegram-community-url]

## Introduction

BeeScreens is a framework allowing any third-party developpers to develop new interactive applications to be streamed over the Internet from one media (i.e. a phone, computer, etc) to any other media both only using a modern Web Browser. See the [BeeScreens official website][beescreens-url] for more details on how BeeScreens works!

## Run BeeScreens Display Web Interface

There are two methods to run the BeeScreens Display Web Interface:

- Using `docker` without installing any other programs (recommended method)
- Locally with all prerequisites

### Using `docker`

*Please refer to the [BeeScreens Docker Files website][docker-files-website] for details on how to use this repository with `docker`.*

The Display Web Interface can be started with:

```sh
docker-compose up beescreens-display-web-interface
```

### Locally

These instructions will get you a copy of the project up and running on your computer.

#### Prerequisites

- [`git`](https://git-scm.com/) must be installed.
- [`Node.js`](https://nodejs.org) must be installed.

#### Clone the repository and configure the environment variables

```sh
# Clone the repository
git clone https://gitlab.com/beescreens/beescreens-display-web-interface.git

# Move to the cloned directory
cd beescreens-display-web-interface/src

# Install the dependencies
npm install

# Copy the environment variables file
cp .env.example .env

# Edit the environment variables file
vim .env
```

#### Start the Display Web Interface

```sh
# Install all the dependencies
npm install

# Start the Display Web Interface
npm run dev
```

The API is now running and can be accessed on [http://localhost:63352](http://localhost:63352)

## Use BeeScreens Display Web Interface

When accessing the BeeScreens Display Web Interface, the page seems quite empty and sad. This is normal, as the page is just used to display content rendered from the application that will stream its content to the displayer.

The icon in top right let you access to the configuration. The configuration allows you to set BeeScreens API endpoint and the API key to access the BeeScreens API. The JWT can be displayed for debug purposes.

However, for remote control, most of the settings can be set from the query parameters:

- `showConfig (boolean)`: Show the configuration panel.
- `apiEndpoint (string)`: Set the BeeScreens API endpoint.
- `apiKey (string)`: Set the API key to access the BeeScreens API.
- `showApiKey (boolean)`: Show the API key field.
- `showJwt (boolean)`: Show the JWT field.
- `submit (boolean)`: Submit the form and try to get the JWT from the BeeScreens API.

If `submit` is set to `true`, the top right icon to access the configuration is not displayed.

## Technologies used

The BeeScreens Display Web Interface uses the following technologies:

- [Node.js](https://nodejs.org/)
- [Next.js](https://nextjs.org/)
- [Material-UI](https://material-ui.com/)
- [next-i18next](https://github.com/isaachinman/next-i18next)
- [dotenv](https://www.npmjs.com/package/dotenv)
- [dotenv-expand](https://www.npmjs.com/package/dotenv-expand)
- [Jest](https://jestjs.io/)
- [Enzyme](https://airbnb.io/enzyme/)
- [ESLint](https://eslint.org/)
- [Prettier](https://prettier.io/)
- [Husky](https://www.npmjs.com/package/husky)
- [sync-dotenv](https://www.npmjs.com/package/sync-dotenv)

## Changelog

Changelog can be found in the [CHANGELOG.md][changelog-url] file.

## License

This project is free and will always be. The source code is licensed under the MIT License - see the [LICENSE.md][license-url]
file for details.

## Contributing

Thank you for considering contributing to BeeScreens! Here is some help to get you started to contribute to the project:

1. Please start by reading our code of conduct available in the [CODE_OF_CONDUCT.md][code-of-conduct-url] file.
2. All contribution information is available in the [CONTRIBUTING.md][contributor-url] file.

Feel free to contribute to the project in any way that you can think of, your contributions are more than welcome!

## Want to reach us?

BeeScreens has the following main channels to communicate:

- [GitLab][repository-url], using [issues][issue-url]
- [BeeScreens' Telegram Channel][telegram-channel-url], where updates are sent when someone is working on BeeScreens
- [BeeScreens' Telegram Community][telegram-community-url], group where discussions are allowed to ask anything related to BeeScreens

Feel free to use any of the communication channels to reach us!

[pipeline-status-badge]: https://gitlab.com/beescreens/beescreens-display-web-interface/badges/master/pipeline.svg
[pipeline-status-url]: https://gitlab.com/beescreens/beescreens-display-web-interface/commits/master

[coverage-report-badge]: https://gitlab.com/beescreens/beescreens-display-web-interface/badges/master/coverage.svg
[coverage-report-url]: https://gitlab.com/beescreens/beescreens-display-web-interface/commits/master

[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license-url]: https://gitlab.com/beescreens/beescreens-display-web-interface/blob/master/LICENSE.md

[code-of-conduct-badge]: https://img.shields.io/badge/code%20of%20conduct%20-Contributor%20Covenant%20v2.0-ff69b4.svg
[code-of-conduct-url]: https://gitlab.com/beescreens/beescreens-display-web-interface/blob/master/CODE_OF_CONDUCT.md

[contributor-url]: https://gitlab.com/beescreens/beescreens-display-web-interface/blob/master/CONTRIBUTING.md

[changelog-badge]: https://img.shields.io/badge/changelog-Keep%20a%20Changelog%20v1.1.0-%23E05735.svg
[changelog-url]: https://gitlab.com/beescreens/beescreens-display-web-interface/blob/master/CHANGELOG.md

[telegram-channel-badge]: https://img.shields.io/badge/telegram-BeeScreens%20Channel-blue.svg
[telegram-channel-url]: https://t.me/beescreens

[telegram-community-badge]: https://img.shields.io/badge/telegram-BeeScreens%20Community-blue.svg
[telegram-community-url]: https://t.me/beescreens_community

[beescreens-url]: https://beescreens.ch
[docker-files-website]: https://docs.docker.beescreens.ch

[repository-url]: https://gitlab.com/beescreens/beescreens-display-web-interface
[issue-url]: https://gitlab.com/beescreens/beescreens-display-web-interface/blob/master/ISSUE_TEMPLATE.md
