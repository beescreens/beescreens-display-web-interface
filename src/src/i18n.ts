import NextI18Next from 'next-i18next';

const NextI18NextInstance = new NextI18Next({
  localePath: typeof window === 'undefined' ? 'public/locales' : 'locales',
  defaultLanguage: 'en',
  otherLanguages: ['fr'],
  defaultNS: 'common',
});

export default NextI18NextInstance;

export const {
  appWithTranslation,
  i18n,
  withTranslation,
} = NextI18NextInstance;
