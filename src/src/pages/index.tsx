import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, IconButton } from '@material-ui/core';

import { useRouter } from 'next/router';

import { Settings } from '@material-ui/icons';
import { withTranslation } from '../i18n';
import ConfigurationDialog from '../components/ConfigurationDialog';

const useStyles = makeStyles(theme => ({
  settingsButton: {
    position: 'absolute',
    margin: theme.spacing(1),
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  displayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    'z-index': '-1',
  },
}));

interface State {
  showIcon: boolean;
  showConfig: boolean;
}

function Index() {
  const classes = useStyles();
  const router = useRouter();

  const { showConfig, submit } = router.query;

  const [configuration, setConfiguration] = useState<State>({
    showConfig: showConfig === 'true',
    showIcon: submit !== 'true',
  });

  const handleShowConfig = () => {
    setConfiguration({ ...configuration, showConfig: true });
  };

  const handleHideConfig = () => {
    setConfiguration({ ...configuration, showConfig: false });
  };

  return (
    <>
      {configuration.showIcon ? (
        <IconButton
          aria-label='settings'
          onClick={handleShowConfig}
          className={classes.settingsButton}
        >
          <Settings fontSize='large' />
        </IconButton>
      ) : (
        ''
      )}
      <ConfigurationDialog
        showConfiguration={configuration.showConfig}
        closeConfiguration={handleHideConfig}
      />
      <CssBaseline />
      <video muted id='displayer' className={classes.displayer} />
    </>
  );
}

Index.getInitialProps = async (): Promise<any> => ({
  namespacesRequired: ['common'],
});

export default withTranslation()(Index);
